from config import tweepy
import config

def initialize(start_id, stop_id, outf):
    auth = tweepy.AppAuthHandler(config.consumer_key, config.consumer_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

    num = getTweetsNonRecursively(start_id, stop_id, outf, api)
    return num

    # first_status = api.get_status(id=start_id, tweet_mode="extended")
    #
    # getTweetsRecursively(first_status, stop_id, outf, api)

def initialize2(start_id, stop_id, outf):
    auth = tweepy.AppAuthHandler(config.consumer_key, config.consumer_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

    first_status = api.get_status(id=start_id, tweet_mode="extended")

    getTweetsRecursively(first_status, stop_id, outf, api)

def getTweetsRecursively(status, stop_id, outf, api):
    tweet_id = status.in_reply_to_status_id

    if (tweet_id != None) and tweet_id != stop_id:
        getTweetsRecursively(api.get_status(id=tweet_id, tweet_mode="extended"), stop_id, outf, api)

    outf.write(status.full_text)
    outf.write("\n\n")


def getTweetsNonRecursively(start_id, stop_id, outf, api):
    lst = []

    curr_id = start_id
    while curr_id != None and curr_id != stop_id:
        status = api.get_status(id=curr_id, tweet_mode="extended")
        lst.insert(0, status.full_text)
        curr_id = status.in_reply_to_status_id

    for i in lst:
        outf.write(i)
        outf.write("\n\n")
    return len(lst)
