import time
from tkinter import *
from tkinter import ttk
from twitterFunctions import initialize, initialize2
from tkmacosx import Button, Radiobutton
import timeit

# Test variables
start_default = 'https://twitter.com/yogawithkats/status/1425540080127692804'
stop_default= ''

# Color variables
ltgrey = '#EAEAEA'
ltpurple = '#CBC4EA'
purple = '#73628A'
navy = '#313D5A'
navyhi = '#415176'
gunmetal = '#183642'

main_button_state = 'active'
about_button_state = 'normal'
def retrieve():
    # Feedback
    state = Label(main_page, text='Working...', padx=5, pady=5, bg=ltpurple, font=('Times', 20))
    state.grid(row=6, columnspan=1, column=1,sticky=W)
    state.update()

    # Text processing
    start_id = start_link.get().split('/')[-1]
    stop_id = stop_link.get()
    save_name = outf_name.get() + '.txt'

    outf = open(save_name, "w")

    if stop_id == "":
        stop_id = None
    else:
        stop_id = int(stop_id.split('/')[-1])

    t1= time.time()
    num = initialize(start_id, stop_id, outf)
    t2 = time.time()
    print("Non-recursive:", t2-t1)
    t1=time.time()
    initialize2(start_id, stop_id, outf)
    t2=time.time()
    print("Recursive:", t2-t1)
    print(num, "tweet thread has been successfully grabbed.")
    # Begin collecting tweets
    # print(timeit.timeit(globals=globals(), setup='start_id=start_id; stop_id=stop_id; outf=outf', stmt='initialize(start_id, stop_id, outf)', number=1))
    # print(timeit.timeit(globals=globals(), setup='start_id=start_id; stop_id=stop_id; outf=outf', stmt='initialize2(start_id, stop_id, outf)',number=1))

    state.grid_remove()
    state = Label(main_page, text='Success!', padx=5, pady=5, bg=ltpurple, font=('Times', 20))
    state.grid(row=6, columnspan=1, column=1,sticky=W)

    outf.close()

# Basics
root = Tk()
root.title('Twitter Thread Scraper')
root.geometry("700x550")
root.config(bg=purple)

# Frames
main_page = Frame(root,bg=ltpurple, pady=50)
about_page = Frame(root, bg=ltpurple)
nav_bar = Frame(root, bg=purple, padx=20, pady=10)

def show_main_page():
    about_page.pack_forget()
    main_page.pack()
    main_button_state = 'pressed'
    about_button_state = 'normal'
def show_about_page():
    main_page.pack_forget()
    about_page.pack()
    main_button_state = 'normal'
    about_button_state = 'pressed'

####################
#### NAVIGATION ####
####################
var = IntVar()

main_button = Radiobutton(nav_bar, text='Main', width=10, height=2, padx=10, value=1, variable=var,
              indicatoron=0, fg=ltgrey, bg=navy, selectcolor=navyhi, font=('Times', 20),
              state=main_button_state, command=show_main_page)
about_button = Radiobutton(nav_bar, text='About', width=10, height=2, padx=10, value=2, variable=var,
               indicatoron=0, fg=ltgrey, bg=navy, selectcolor=navyhi, font=('Times', 20),
               state=about_button_state, command=show_about_page)

main_button.pack(side=LEFT, padx=5, pady=5)
about_button.pack(side=LEFT, padx=5,pady=5)

main_button.select()
nav_bar.pack()

###################
#### MAIN PAGE ####
###################

# Labels
start_label = Label(main_page, text='Enter Link to Last Tweet', padx=5, pady=5, bg=ltpurple, font=('Times', 16))
start_label.grid(row=1, column=0, sticky=W, pady=20)

outf_name_label= Label(main_page, text='Save File Name', padx=5, pady=5, bg=ltpurple, font=('Times', 16))
outf_name_label.grid(row=2, column=0, sticky=W, pady=20)

stop_name = Label(main_page, text='Link to Beginning Tweet (optional)', padx=5, pady=5, bg=ltpurple, font=('Times', 16))
stop_name.grid(row=3, column=0, sticky=W, pady=20)

# User input
start_link = Entry(main_page, width = 45)
start_link.insert(0, start_default)
start_link.grid(row=1, column=1, pady=20, padx=20)

outf_name = Entry(main_page, width = 45)
outf_name.insert(0,'IrohQuotes')
outf_name.grid(row=2, column=1, pady=20, padx=20)

stop_link = Entry(main_page, width=45)
stop_link.insert(0, stop_default)
stop_link.grid(row=3, column=1, pady=20, padx=20)

# Submit button
submit = Button(main_page, text='Submit', width=110, height=50, bg=navy, fg=ltgrey, borderless=True,\
padx=10, pady=10, font=('Times', 18), command = retrieve)
submit.grid(row=4, column=1, pady=5)

main_page.pack()

####################
#### ABOUT PAGE ####
####################

about_text_file = open('about_text.txt').read()

about_text = Label(about_page, wraplength=600, bg=navy, fg=ltgrey, padx=5, pady=5, font=('Times', 15),\
text= about_text_file)

about_text.pack()

###################
###################

root.mainloop()
